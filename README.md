81digits.com
======

## Installation

### Docker Dotenv example

```dotenv
# ./docker/.env

DATABASE_NAME=81digits
DATABASE_USER=81digits
DATABASE_PASSWORD=password
DATABASE_ROOT_PASSWORD=password
```

### Symfony Dotenv example

```dotenv
# ./application/.env.local
DATABASE_URL="mysql://81digits:password@database:3306/81digits?serverVersion=mariadb-10.5.10"
```

### Build and start/stop docker

```bash
# For development (with shared volume)
$ docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up -d --build
$ docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml down

# For production
$ docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml up -d --build
$ docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml down
```

