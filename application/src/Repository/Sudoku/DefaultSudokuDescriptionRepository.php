<?php

namespace App\Repository\Sudoku;

use App\Entity\Sudoku\DefaultSudokuDescription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DefaultSudokuDescription|null find($id, $lockMode = null, $lockVersion = null)
 * @method DefaultSudokuDescription|null findOneBy(array $criteria, array $orderBy = null)
 * @method DefaultSudokuDescription[]    findAll()
 * @method DefaultSudokuDescription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefaultSudokuDescriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DefaultSudokuDescription::class);
    }
}
