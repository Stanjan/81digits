<?php

namespace App\Repository\Sudoku;

use App\Entity\Sudoku\DefaultSudokuSeed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DefaultSudokuSeed|null find($id, $lockMode = null, $lockVersion = null)
 * @method DefaultSudokuSeed|null findOneBy(array $criteria, array $orderBy = null)
 * @method DefaultSudokuSeed[]    findAll()
 * @method DefaultSudokuSeed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefaultSudokuSeedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DefaultSudokuSeed::class);
    }
}
