<?php

namespace App\Entity\Sudoku;

use Stanjan\Sudoku\BelongsToSudokuVariantInterface;

/**
 * Seed for an \Stanjan\Sudoku\SudokuInterface::class instance.
 */
interface SudokuSeedInterface extends BelongsToSudokuVariantInterface
{
    public function getId(): ?int;

    /**
     * Returns the type of this seed defined in \App\Sudoku\SudokuSeedType::class.
     */
    public static function getType(): string;

    /**
     * Returns the description class name for this seed.
     */
    public static function getDescriptionClassName(): string;

    /**
     * The answers for the sudoku in a string from left to right, top to bottom, using a zero for unknown.
     */
    public function getAnswers(): string;
    public function setAnswers(string $answers);

    /**
     * The solutions for the sudoku in a string from left to right, top to bottom.
     */
    public function getSolutions(): string;
    public function setSolutions(string $solutions);

    /**
     * The difficulty level.
     */
    public function getDifficultyLevel(): int;
    public function setDifficultyLevel(int $difficultyLevel);

    /**
     * The difficulty rating of the sudoku.
     */
    public function getDifficultyRating(): int;
    public function setDifficultyRating(int $difficultyRating);

    /**
     * The datetime the sudoku was created.
     */
    public function getCreatedAt(): \DateTimeInterface;
}