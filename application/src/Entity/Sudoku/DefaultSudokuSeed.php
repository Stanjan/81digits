<?php

namespace App\Entity\Sudoku;

use App\Repository\Sudoku\DefaultSudokuSeedRepository;
use App\Sudoku\SudokuSeedType;
use Doctrine\ORM\Mapping as ORM;
use Stanjan\Sudoku\Variant\Default\DefaultSudokuVariant;

/**
 * Seed for \Stanjan\Sudoku\Variant\Default\DefaultSudoku::class.
 */
#[ORM\Entity(DefaultSudokuSeedRepository::class)]
#[ORM\Table('sudoku_default_seed')]
class DefaultSudokuSeed implements SudokuSeedInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 81, unique: true)]
    private string $answers;

    #[ORM\Column(type: 'string', length: 81, nullable: true)]
    private string $solutions;

    /**
     * The difficulty level from \Stanjan\Sudoku\Variant\Default\Solver\SolvingMethod::class.
     */
    #[ORM\Column(type: 'integer')]
    private int $difficultyLevel;

    #[ORM\Column(type: 'integer')]
    private int $difficultyRating;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * {@inheritdoc}
     */
    public static function getType(): string
    {
        return SudokuSeedType::DEFAULT;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDescriptionClassName(): string
    {
        return DefaultSudokuDescription::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getVariantClassName(): string
    {
        return DefaultSudokuVariant::class;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAnswers(): string
    {
        return $this->answers;
    }

    public function setAnswers(string $answers): void
    {
        $this->answers = $answers;
    }

    /**
     * {@inheritdoc}
     */
    public function getSolutions(): string
    {
        return $this->solutions;
    }

    public function setSolutions(string $solutions): void
    {
        $this->solutions = $solutions;
    }

    /**
     * {@inheritdoc}
     */
    public function getDifficultyLevel(): int
    {
        return $this->difficultyLevel;
    }

    public function setDifficultyLevel(int $difficultyLevel): void
    {
        $this->difficultyLevel = $difficultyLevel;
    }

    /**
     * {@inheritdoc}
     */
    public function getDifficultyRating(): int
    {
        return $this->difficultyRating;
    }

    public function setDifficultyRating(int $difficultyRating): void
    {
        $this->difficultyRating = $difficultyRating;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
