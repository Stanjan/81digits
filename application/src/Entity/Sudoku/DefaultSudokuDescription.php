<?php

namespace App\Entity\Sudoku;

use App\Repository\Sudoku\DefaultSudokuDescriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description for DefaultSudokuSeed::class.
 */
#[ORM\Entity(DefaultSudokuDescriptionRepository::class)]
#[ORM\Table('sudoku_default_description')]
class DefaultSudokuDescription implements SudokuDescriptionInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\ManyToOne(DefaultSudokuSeed::class)]
    private DefaultSudokuSeed $seed;

    #[ORM\Column(type: 'string', length: 81)]
    private string $answers;

    #[ORM\Column(type: 'string', length: 81, nullable: true)]
    private string $solutions;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getSeed(): DefaultSudokuSeed
    {
        return $this->seed;
    }

    public function setSeed(DefaultSudokuSeed $seed): void
    {
        $this->seed = $seed;
    }

    /**
     * {@inheritdoc}
     */
    public function getAnswers(): string
    {
        return $this->answers;
    }

    public function setAnswers(string $answers): void
    {
        $this->answers = $answers;
    }

    /**
     * {@inheritdoc}
     */
    public function getSolutions(): string
    {
        return $this->solutions;
    }

    public function setSolutions(string $solutions): void
    {
        $this->solutions = $solutions;
    }
}
