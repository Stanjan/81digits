<?php

namespace App\Entity\Sudoku;

use Stanjan\Sudoku\BelongsToSudokuVariantInterface;

/**
 * Description for an \Stanjan\Sudoku\SudokuInterface::class instance.
 */
interface SudokuDescriptionInterface
{
    /**
     * The seed for this description.
     */
    public function getSeed(): SudokuSeedInterface;

    /**
     * The answers for the sudoku in a string from left to right, top to bottom, using a zero for unknown.
     */
    public function getAnswers(): string;
    public function setAnswers(string $answers);

    /**
     * The solutions for the sudoku in a string from left to right, top to bottom.
     */
    public function getSolutions(): string;
    public function setSolutions(string $solutions);
}