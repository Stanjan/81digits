<?php

namespace App\Sudoku;

use App\Entity\Sudoku\DefaultSudokuSeed;
use InvalidArgumentException;

/**
 * Enum containing sudoku seed types.
 */
class SudokuSeedType
{
    const DEFAULT = 'default';

    /**
     * Returns the class name for the given seed type.
     *
     * @throws InvalidArgumentException Thrown when the type was unknown.
     */
    public static function getSeedClassNameForType(string $type): string
    {
        switch ($type) {
            case self::DEFAULT:
                return DefaultSudokuSeed::class;
        }

        throw new InvalidArgumentException(sprintf('Sudoku seed for type "%s" not found.', $type));
    }
}