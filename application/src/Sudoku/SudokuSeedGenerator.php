<?php

namespace App\Sudoku;

use App\Entity\Sudoku\SudokuSeedInterface;
use App\Storage\SudokuStorage;
use InvalidArgumentException;
use Stanjan\Sudoku\Exception\GeneratorException;
use Stanjan\Sudoku\SudokuInterface;
use Stanjan\Sudoku\SudokuVariantInterface;
use Stanjan\Sudoku\Variant\Default\Solver\SolvingMethod;

class SudokuSeedGenerator
{
    const RETRY_UNIQUE_LIMIT = 10;

    public function __construct(
        private SudokuStorage $storage,
    ) {}

    /**
     * Generates an unique seed for the given seed type.
     *
     * @param string $type Seed type from SudokuSeedType::class.
     */
    public function generateUniqueSeed(string $type, ?int $difficultyLevel = null): SudokuSeedInterface
    {
        if ($type === SudokuSeedType::DEFAULT) {
            if (null === $difficultyLevel) {
                // Set random difficulty level.
                $difficultyLevels = [
                    SolvingMethod::DIFFICULTY_EASY,
                    SolvingMethod::DIFFICULTY_MEDIUM,
                    SolvingMethod::DIFFICULTY_HARD,
                    SolvingMethod::DIFFICULTY_VERY_HARD,
                ];
                $difficultyLevel = $difficultyLevels[array_rand($difficultyLevels)];
            }
            $allowedSolvingMethods = SolvingMethod::getForDifficultyLevel($difficultyLevel);
        } else {
            throw new InvalidArgumentException(sprintf('Sudoku generator for type "%s" is not supported.', $type));
        }

        $className = SudokuSeedType::getSeedClassNameForType($type);
        /** @var SudokuSeedInterface $seed */
        $seed = new $className;
        $sudoku = $this->generateUniqueSudoku($seed, $allowedSolvingMethods);

        $seed->setAnswers(SudokuSerializer::serializeAnswers($sudoku));
        $seed->setSolutions(SudokuSerializer::serializeSolutions($sudoku));
        $seed->setDifficultyRating($sudoku->getDifficultyRating());
        $seed->setDifficultyLevel($difficultyLevel);

        return $seed;
    }

    /**
     * Generates an unique sudoku for the given seed.
     */
    private function generateUniqueSudoku(SudokuSeedInterface $seed, array $allowedSolvingMethods = []): SudokuInterface
    {
        /** @var SudokuVariantInterface $variant */
        $variant = new ($seed::getVariantClassName());
        $generator = $variant->getGenerator();

        for ($attempt = 0; $attempt < self::RETRY_UNIQUE_LIMIT; $attempt++) {
            $sudoku = $generator->generate();
            $answers = SudokuSerializer::serializeAnswers($sudoku);
            if (!$this->storage->seedExists($seed::getType(), $answers)) {
                return $sudoku;
            }
        }

        throw new GeneratorException(sprintf('No unique sudoku could be generated in %d attempts.', self::RETRY_UNIQUE_LIMIT));
    }
}