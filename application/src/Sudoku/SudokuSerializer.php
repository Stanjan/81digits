<?php

namespace App\Sudoku;

use Stanjan\Sudoku\SudokuInterface;

class SudokuSerializer
{
    /**
     * Value used in serialization when an answer/solution is not set.
     */
    const EMPTY_VALUE = '0';

    const TYPE_ANSWERS = 'answers';
    const TYPE_SOLUTIONS = 'solutions';

    public static function serializeAnswers(SudokuInterface $sudoku): string
    {
        return self::serializeCollection($sudoku, self::TYPE_ANSWERS);
    }

    public static function serializeSolutions(SudokuInterface $sudoku): string
    {
        return self::serializeCollection($sudoku, self::TYPE_SOLUTIONS);
    }

    /**
     * Serializes the answers or solutions collection of a sudoku.
     */
    private static function serializeCollection(SudokuInterface $sudoku, string $type = self::TYPE_ANSWERS): string
    {
        if (!in_array($type, [self::TYPE_ANSWERS, self::TYPE_SOLUTIONS])) {
            throw new \InvalidArgumentException(sprintf('Invalid type "%s".', $type));
        }

        $gridSize = $sudoku->getGrid()->getSize();

        $seed = '';

        for ($row = 1; $row <= $gridSize->getRowCount(); $row++) {
            for ($column = 1; $column <= $gridSize->getColumnCount(); $column++) {
                $answer = $type === self::TYPE_SOLUTIONS
                    ? $sudoku->getSolution($row, $column)
                    : $sudoku->getAnswer($row, $column);

                $seed .= $answer ?: self::EMPTY_VALUE;
            }
        }

        return $seed;
    }
}