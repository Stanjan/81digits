<?php

namespace App\Storage;

use App\Entity\Sudoku\SudokuSeedInterface;
use App\Sudoku\SudokuSeedType;
use Doctrine\ORM\EntityManagerInterface;

class SudokuStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {}

    /**
     * Checks if a seed for the given type with the given answers exists.
     *
     * @param string $type Seed type from SudokuSeedType::class.
     */
    public function seedExists(string $type, string $answers): bool
    {
        $className = SudokuSeedType::getSeedClassNameForType($type);
        $repository = $this->entityManager->getRepository($className);
        $seed = $repository->findOneBy(['answers' => $answers]);

        return null !== $seed;
    }

    /**
     * Stores the given seed into the database.
     */
    public function storeSeed(SudokuSeedInterface $seed): void
    {
        $this->entityManager->persist($seed);
        $this->entityManager->flush();
    }

    /**
     * Stores all possible descriptions for the given seed.
     */
//    public function storeDescriptionsForSeed(SudokuSeedInterface $seed): void
//    {
//        if ($seed->getDescriptionCount() > 0) {
//            throw new \LogicException('The given seed already has descriptions.');
//        }
//
//        // Replace the answer & solution numbers with letters (123456789 will be abcdefghi).
//        $answers = $seed->getAnswers();
//        $solutions = $seed->getSolutions();
//        $alphabet = range('a', 'i');
//        $numberCount = sqrt(strlen($answers));
//        $usedAlphabet = array_slice($alphabet, 0, $numberCount);
//        for ($number = 1; $number <= $numberCount; $number++) {
//            $answers = str_replace($number, $alphabet[$number - 1], $answers);
//            $solutions = str_replace($number, $alphabet[$number - 1], $solutions);
//        }
//
//        // Set the answers and solutions in an array.
//        $answersArray = [];
//        foreach (str_split($answers, $numberCount) as $subAnswers) {
//            $answersArray[] = str_split($subAnswers, 1);
//        }
//        $solutionsArray = [];
//        foreach (str_split($solutions, $numberCount) as $subSolutions) {
//            $solutionsArray[] = str_split($subSolutions, 1);
//        }
//
//        // Map all possible answer & solution combinations.
//        $answerCombinations = [$answers];
//        $solutionCombinations = [$solutions];
//
//        // Flip horizontal.
//        $answersArrayFlippedHorizontal = $this->flipAnswersArrayHorizontal($answersArray);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArrayFlippedHorizontal);
//        $solutionsArrayFlippedHorizontal = $this->flipAnswersArrayHorizontal($solutionsArray);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArrayFlippedHorizontal);
//
//        // Flip vertical.
//        $answersArrayFlippedVertical = $this->flipAnswersArrayVertical($answersArray);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArrayFlippedVertical);
//        $solutionsArrayFlippedVertical = $this->flipAnswersArrayVertical($solutionsArray);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArrayFlippedVertical);
//
//        // Flip horizontal & vertical.
//        $answersArrayFlipped = $this->flipAnswersArrayVertical($answersArrayFlippedHorizontal);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArrayFlipped);
//        $solutionsArrayFlipped = $this->flipAnswersArrayVertical($solutionsArrayFlippedHorizontal);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArrayFlipped);
//
//        // Rotate 90 degrees.
//        $answersArray90degrees = $this->rotateAnswersArray90degrees($answersArray);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArray90degrees);
//        $solutionsArray90degrees = $this->rotateAnswersArray90degrees($solutionsArray);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArray90degrees);
//
//        // Rotate 90 degrees & flip horizontal.
//        $answersArray90degreesFlippedHorizontal = $this->flipAnswersArrayHorizontal($answersArray90degrees);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArray90degreesFlippedHorizontal);
//        $solutionsArray90degreesFlippedHorizontal = $this->flipAnswersArrayHorizontal($solutionsArray90degrees);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArray90degreesFlippedHorizontal);
//
//        // Rotate 90 degrees & flip vertical.
//        $answersArray90degreesFlippedVertical = $this->flipAnswersArrayVertical($answersArray90degrees);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArray90degreesFlippedVertical);
//        $solutionsArray90degreesFlippedVertical = $this->flipAnswersArrayVertical($solutionsArray90degrees);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArray90degreesFlippedVertical);
//
//        // Rotate 90 degrees & flip horizontal & vertical.
//        $answersArray90degreesFlipped = $this->flipAnswersArrayVertical($answersArray90degreesFlippedHorizontal);
//        $answerCombinations[] = $this->flattenAnswersArray($answersArray90degreesFlipped);
//        $solutionsArray90degreesFlipped = $this->flipAnswersArrayVertical($solutionsArray90degreesFlippedHorizontal);
//        $solutionCombinations[] = $this->flattenAnswersArray($solutionsArray90degreesFlipped);
//
//        // Create descriptions for all permutations of the array.
//        $permutations = $this->getPermutations(range(1, $numberCount));
//
//        // Create all combinations for all permutations.
//        $metadata = $this->entityManager->getClassMetadata($seed::getDescriptionClassName());
//        $tableName = $metadata->getTableName();
//
//        $insertSqlPrefix = 'INSERT INTO '.$tableName.' (seed_id, answers, solutions) VALUES ';
//        $valuesSql = '';
//        $descriptionCount = 0;
//
//        // Disable the Doctrine SQL Logger for memory.
//        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
//
//        foreach ($permutations as $key => $permutation) {
//            for ($combinationIndex = 0; $combinationIndex < count($answerCombinations); $combinationIndex++) {
//                if ($valuesSql !== '') {
//                    $valuesSql .= ',';
//                }
//
//                $answers = str_replace($usedAlphabet, $permutation, $answerCombinations[$combinationIndex]);
//                $solutions = str_replace($usedAlphabet, $permutation, $solutionCombinations[$combinationIndex]);
//
//                $valuesSql .= '('.$seed->getId().', "'.$answers.'", "'.$solutions.'")';
//                $descriptionCount++;
//                if ($descriptionCount % 1000 === 0) {
//                    // Execute every 1000 values.
//                    $valuesSql .= ';';
//                    $this->entityManager->getConnection()->executeStatement($insertSqlPrefix.$valuesSql);
//                    $valuesSql = '';
//                }
//            }
//        }
//
//        $valuesSql .= ';';
//        $this->entityManager->getConnection()->executeStatement($insertSqlPrefix.$valuesSql);
//    }

//    /**
//     * Returns all permutations of the given numbers array.
//     *
//     * @param array<int> $numbers
//     *
//     * @return \Generator<array<int>>
//     */
//    private function getPermutations(array $numbers): \Generator
//    {
//        if (count($numbers) <= 1) {
//            yield $numbers;
//        } else {
//            foreach ($this->getPermutations(array_slice($numbers, 1)) as $permutation) {
//                foreach (range(0, count($numbers) - 1) as $i) {
//                    yield array_merge(
//                        array_slice($permutation, 0, $i),
//                        [$numbers[0]],
//                        array_slice($permutation, $i)
//                    );
//                }
//            }
//        }
//    }

//    /**
//     * @param array<array<int>> $answersArray
//     *
//     * @return array<array<int>>
//     */
//    private function rotateAnswersArray90degrees(array $answersArray): array
//    {
//        array_unshift($answersArray, null);
//        $answersArray = call_user_func_array('array_map', $answersArray);
//        return array_map('array_reverse', $answersArray);
//    }
//
//    /**
//     * @param array<array<int>> $answersArray
//     *
//     * @return array<array<int>>
//     */
//    private function flipAnswersArrayHorizontal(array $answersArray)
//    {
//        return array_map('array_reverse', $answersArray);
//    }
//
//    /**
//     * @param array<array<int>> $answersArray
//     *
//     * @return array<array<int>>
//     */
//    private function flipAnswersArrayVertical(array $answersArray): array
//    {
//        return array_reverse($answersArray);
//    }
//
//    /**
//     * @param array<array<int>> $answersArray
//     */
//    private function flattenAnswersArray(array $answersArray): string
//    {
//        $answersArray = array_merge(...$answersArray);
//        return implode('', $answersArray);
//    }
}