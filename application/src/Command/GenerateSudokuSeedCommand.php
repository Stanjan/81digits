<?php

namespace App\Command;

use App\Storage\SudokuStorage;
use App\Sudoku\SudokuSeedGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSudokuSeedCommand  extends Command
{
    use LockableTrait;

    public function __construct(
        private SudokuSeedGenerator $seedGenerator,
        private SudokuStorage $storage,
    ) {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:sudoku:generate')
            ->setDescription('Generates a new sudoku seed.')
            ->setHelp('This command generates a new sudoku seed and generates all possible descriptions for it.')
            ->addArgument('type', InputArgument::REQUIRED, 'The sudoku type.')
            ->addArgument('amount', InputArgument::OPTIONAL, 'The amount of seeds to generate.')
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $type */
        $type = $input->getArgument('type');
        if (!$this->lock($this->getName().$type)) {
            $output->writeln('The command is already running in another process for the same type.');

            return Command::SUCCESS;
        }

        $startTime = microtime(true);

        $amount = (int) $input->getArgument('amount');
        if ($amount < 1) {
            $amount = 1;
        }

        $output->writeln([
            'Sudoku seed generator', 
            '============',
        ]);

        $output->writeln(sprintf('Generating %d sudoku(s) with an unique seed...', $amount));

        $progressBar = new ProgressBar($output, $amount);
        $progressBar->start();

        for ($seedCount = 0; $seedCount < $amount; $seedCount++) {
            $seed = $this->seedGenerator->generateUniqueSeed($type);
            $this->storage->storeSeed($seed);
            $progressBar->advance();
        }

        $progressBar->finish();
        $output->writeln('');

        $endTime = microtime(true);
        $output->writeln(sprintf('Total process time: %s seconds.', number_format((float) ($endTime - $startTime), 1, '.', '')));

        $this->release();

        return Command::SUCCESS;
    }
}